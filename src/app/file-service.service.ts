import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileServiceService {
  private httpClient: HttpClient
  private fileApi = 'http://localhost:5000/api/members/';
  constructor() { }
  public getFilesList(){
    return this.httpClient.get(this.fileApi);
  }
}

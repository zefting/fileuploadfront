import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilelistComponent } from './filelist/filelist.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';

const routes: Routes =
[
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'fileupload', component: UploadfileComponent  },
  { path: 'filelist', component: FilelistComponent  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpEventType, HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-uploadfile',
  templateUrl: './uploadfile.component.html',
  styleUrls: ['./uploadfile.component.css']
})
export class UploadfileComponent implements OnInit {
  file: any;
  progress: number;
  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
  }
  onSelectFile($event, file)
  {
    this.file = file;
    console.log(this.file);
  }
  uploadImage()
  {
    if(this.file.length ===0)
    {
      return;
    }
    const formData = new FormData();

    for(let file of this.file)
    formData.append(file.name, file);

    const uploadReq = new HttpRequest('POST', `https://localhost:5001/api/UploadFile`, formData, {
      reportProgress: true,
    });

    this._http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
      {
        this.progress = Math.round(100 * event.loaded / event.total);
      }
    });
  }
}

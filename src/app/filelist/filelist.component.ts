import { Component, OnInit } from '@angular/core';
import { FileServiceService } from './../file-service.service'
@Component({
  selector: 'app-filelist',
  templateUrl: './filelist.component.html',
  styleUrls: ['./filelist.component.css']
})
export class FilelistComponent implements OnInit {
  fileList: any[];
  constructor(private _FileService: FileServiceService) { }
  fileName:any;
  ngOnInit(): void {
    this._FileService.getFilesList().subscribe((mIddata: any[]) => {
      this.fileList = mIddata;
      console.log('FileNameList: ', this.fileList);
      });
  }

}
